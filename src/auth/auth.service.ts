import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '../core/interfaces/jwt-payload';
import { UsersService } from '../core/services/users.service';
import { User } from '../data/entities/user';
import { UserLoginDTO } from '../models/user/user-login-dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
  ) {}

  async signIn(user: UserLoginDTO): Promise<string> {
    const userFound = await this.usersService.signIn(user);
    if (userFound) {
      return await this.jwtService.sign({
        email: userFound.email,
        name: userFound.name,
        id: userFound.id,
        roles: userFound.roles,
      });
    }

    return null;
  }

  async validateUser(payload: JwtPayload): Promise<User | undefined> {
    return await this.usersService.validate(payload);
  }
}
