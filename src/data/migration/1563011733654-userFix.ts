import {MigrationInterface, QueryRunner} from "typeorm";

export class userFix1563011733654 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `activities` (`id` varchar(36) NOT NULL, `modifyingUserId` varchar(255) NOT NULL, `modifyingUserName` varchar(255) NOT NULL, `featureType` enum ('customer', 'outlet', 'user') NOT NULL, `actionType` enum ('created', 'deleted', 'updated') NOT NULL, `itemId` varchar(255) NOT NULL, `itemName` varchar(255) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `version` int NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `roles` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users` (`id` varchar(36) NOT NULL, `name` varchar(255) NOT NULL, `password` varchar(255) NOT NULL, `email` varchar(255) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `version` int NOT NULL, `outletId` varchar(36) NULL, `brandId` varchar(36) NULL, UNIQUE INDEX `IDX_97672ac88f789774dd47f7c8be` (`email`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `outlets` (`id` varchar(36) NOT NULL, `location` varchar(255) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `version` int NOT NULL, `brandId` varchar(36) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `customers` (`id` varchar(36) NOT NULL, `brandName` varchar(255) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `isDeleted` tinyint NOT NULL DEFAULT 0, `version` int NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `prizes` (`barcode` varchar(255) NOT NULL, `prize` varchar(255) NOT NULL, `count` int NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `version` int NOT NULL, PRIMARY KEY (`barcode`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `redemptionRecords` (`id` varchar(36) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `version` int NOT NULL, `barcodeId` varchar(255) NULL, `brandId` varchar(36) NULL, `outletId` varchar(36) NULL, `userId` varchar(36) NULL, `prizeBarcode` varchar(255) NULL, UNIQUE INDEX `REL_0dbea2509b98b01d251fb6a878` (`barcodeId`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `barcodes` (`id` varchar(255) NOT NULL, `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), `version` int NOT NULL, `status` varchar(255) NOT NULL DEFAULT 'Valid', PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `users_roles_roles` (`usersId` varchar(36) NOT NULL, `rolesId` varchar(36) NOT NULL, INDEX `IDX_df951a64f09865171d2d7a502b` (`usersId`), INDEX `IDX_b2f0366aa9349789527e0c36d9` (`rolesId`), PRIMARY KEY (`usersId`, `rolesId`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_44557630e787a2fc078722b3351` FOREIGN KEY (`outletId`) REFERENCES `outlets`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_b245dea25ee35b6a6e48f1c743a` FOREIGN KEY (`brandId`) REFERENCES `customers`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `outlets` ADD CONSTRAINT `FK_66086c87aef07bb4aa8846ba84f` FOREIGN KEY (`brandId`) REFERENCES `customers`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemptionRecords` ADD CONSTRAINT `FK_0dbea2509b98b01d251fb6a8786` FOREIGN KEY (`barcodeId`) REFERENCES `barcodes`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemptionRecords` ADD CONSTRAINT `FK_90a3309c0554d74b85b3bc14da3` FOREIGN KEY (`brandId`) REFERENCES `customers`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemptionRecords` ADD CONSTRAINT `FK_ab593268faf5e832760a6df651b` FOREIGN KEY (`outletId`) REFERENCES `outlets`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemptionRecords` ADD CONSTRAINT `FK_1f92cd37a4f6f2531c2072bf9f3` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `redemptionRecords` ADD CONSTRAINT `FK_d03ef42a21a97fb0ace1e7a5bb5` FOREIGN KEY (`prizeBarcode`) REFERENCES `prizes`(`barcode`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users_roles_roles` ADD CONSTRAINT `FK_df951a64f09865171d2d7a502b1` FOREIGN KEY (`usersId`) REFERENCES `users`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users_roles_roles` ADD CONSTRAINT `FK_b2f0366aa9349789527e0c36d97` FOREIGN KEY (`rolesId`) REFERENCES `roles`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `users_roles_roles` DROP FOREIGN KEY `FK_b2f0366aa9349789527e0c36d97`");
        await queryRunner.query("ALTER TABLE `users_roles_roles` DROP FOREIGN KEY `FK_df951a64f09865171d2d7a502b1`");
        await queryRunner.query("ALTER TABLE `redemptionRecords` DROP FOREIGN KEY `FK_d03ef42a21a97fb0ace1e7a5bb5`");
        await queryRunner.query("ALTER TABLE `redemptionRecords` DROP FOREIGN KEY `FK_1f92cd37a4f6f2531c2072bf9f3`");
        await queryRunner.query("ALTER TABLE `redemptionRecords` DROP FOREIGN KEY `FK_ab593268faf5e832760a6df651b`");
        await queryRunner.query("ALTER TABLE `redemptionRecords` DROP FOREIGN KEY `FK_90a3309c0554d74b85b3bc14da3`");
        await queryRunner.query("ALTER TABLE `redemptionRecords` DROP FOREIGN KEY `FK_0dbea2509b98b01d251fb6a8786`");
        await queryRunner.query("ALTER TABLE `outlets` DROP FOREIGN KEY `FK_66086c87aef07bb4aa8846ba84f`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_b245dea25ee35b6a6e48f1c743a`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_44557630e787a2fc078722b3351`");
        await queryRunner.query("DROP INDEX `IDX_b2f0366aa9349789527e0c36d9` ON `users_roles_roles`");
        await queryRunner.query("DROP INDEX `IDX_df951a64f09865171d2d7a502b` ON `users_roles_roles`");
        await queryRunner.query("DROP TABLE `users_roles_roles`");
        await queryRunner.query("DROP TABLE `barcodes`");
        await queryRunner.query("DROP INDEX `REL_0dbea2509b98b01d251fb6a878` ON `redemptionRecords`");
        await queryRunner.query("DROP TABLE `redemptionRecords`");
        await queryRunner.query("DROP TABLE `prizes`");
        await queryRunner.query("DROP TABLE `customers`");
        await queryRunner.query("DROP TABLE `outlets`");
        await queryRunner.query("DROP INDEX `IDX_97672ac88f789774dd47f7c8be` ON `users`");
        await queryRunner.query("DROP TABLE `users`");
        await queryRunner.query("DROP TABLE `roles`");
        await queryRunner.query("DROP TABLE `activities`");
    }

}
