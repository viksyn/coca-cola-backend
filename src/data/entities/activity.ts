import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { ActivityType } from '../../common/enums/activity-type.enum';
import { FeatureType } from '../../common/enums/feature-type.enum';

@Entity('activities')
export class Activity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  modifyingUserId: string;

  @Column('nvarchar')
  modifyingUserName: string;

  @Column({
    type: 'enum',
    enum: FeatureType,
  })
  featureType: FeatureType;

  @Column({
    type: 'enum',
    enum: ActivityType,
  })
  actionType: ActivityType;

  @Column('nvarchar')
  itemId: string;

  @Column('nvarchar')
  itemName: string;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @VersionColumn()
  version: number;
}
