import * as bcrypt from 'bcrypt';
import { createConnection, Repository } from 'typeorm';
import { PrizeType } from '../../common/enums/prize.enum';
import { UserRole } from '../../common/enums/user-role.enum';
import { ConfigService } from '../../config/config.service';
import { Barcode } from '../entities/barcode';
import { Prize } from '../entities/prize';
import { Role } from '../entities/role';
import { User } from '../entities/user';

// run: `npm run seed` to seed the database

const seedRoles = async connection => {
  const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

  const roles: Role[] = await rolesRepo.find();
  if (roles.length) {
    console.log('The DB already has roles!');
    return;
  }

  const rolesSeeding: Array<Promise<Role>> = Object.keys(UserRole).map(
    async (roleName: string) => {
      const role: Role = rolesRepo.create({ name: roleName });
      return await rolesRepo.save(role);
    },
  );
  await Promise.all(rolesSeeding);

  console.log('Seeded roles successfully!');
};

const seedAdmin = async connection => {
  const userRepo: Repository<User> = connection.manager.getRepository(User);
  const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);
  const configService = new ConfigService();

  const admin = await userRepo.findOne({
    where: {
      name: 'admin',
    },
  });

  if (admin) {
    console.log('The DB already has an admin!');
    return;
  }

  const adminRole: Role = await rolesRepo.findOne({ name: UserRole.Admin });
  if (!adminRole) {
    console.log('The DB does not have an admin role!');
    return;
  }

  const newAdmin: User = userRepo.create({
    name: 'admin',
    email: 'admin@admin.com',
    password: bcrypt.hashSync(configService.dbAdmin, 10),
    roles: [adminRole],
  });

  await userRepo.save(newAdmin);
  console.log('Seeded admin successfully!');
};

const seedBarcode = async connection => {
  const barcodeRepo: Repository<Barcode> = connection.manager.getRepository(
    Barcode,
  );
  const winningBarcodesCount = 50;
  const barcodeFirstEight = '38090000';
  const barcodes = [];
  for (let i = 0; i < winningBarcodesCount; i++) {
    const barcodeLastFive = Math.floor(Math.random() * 90000) + 10000;
    const barcode = barcodeRepo.create({
      id: barcodeFirstEight + barcodeLastFive,
    });
    barcodes.push(barcode);
  }
  await barcodeRepo.save(barcodes);
  console.log('Seeded winning barcodes successfully');
};

const seedPrizes = async connection => {
  const prizeRepo: Repository<Prize> = connection.manager.getRepository(Prize);

  const prizeType = PrizeType;
  const prizeTypeArr = Object.values(prizeType);
  const barcodeFirstEight = '54490000';
  const prizeBarcodes = [];
  for (const prize of prizeTypeArr) {
    const prizeCount = Math.floor(Math.random() * 20 + 1);
    const barcodeLastFive = Math.floor(Math.random() * 90000) + 10000;

    const prizeRecord = prizeRepo.create({
      barcode: barcodeFirstEight + barcodeLastFive,
      prize,
      count: prizeCount,
    });

    prizeBarcodes.push(prizeRecord);
  }
  await prizeRepo.save(prizeBarcodes);
};

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  await seedRoles(connection);
  await seedAdmin(connection);
  await seedBarcode(connection);
  await seedPrizes(connection);

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);
