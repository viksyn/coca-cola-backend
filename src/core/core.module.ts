import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Activity } from '../data/entities/activity';
import { Barcode } from '../data/entities/barcode';
import { Customer } from '../data/entities/customer';
import { Outlet } from '../data/entities/outlet';
import { Prize } from '../data/entities/prize';
import { RedemptionRecord } from '../data/entities/redemptionRecord';
import { Role } from '../data/entities/role';
import { User } from '../data/entities/user';
import { MailerService } from './services/mailer.service';
import { MappingService } from './services/mapping.service';
import { UsersService } from './services/users.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      Role,
      Outlet,
      Customer,
      RedemptionRecord,
      Barcode,
      Prize,
      Activity,
    ]),
  ],
  providers: [UsersService, MappingService, MailerService],
  exports: [UsersService, TypeOrmModule, MailerService],
})
export class CoreModule {}
