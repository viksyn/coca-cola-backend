import { Injectable } from '@nestjs/common';
import * as nodemailer from 'nodemailer';

@Injectable()
export class MailerService {
  private transporter;

  public constructor() {
    const main = async () => {
      const testAccount = await nodemailer.createTestAccount();

      this.transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'redeemprojectcola',
          pass: 'User123!',
        },
      });
    };

    main().catch(console.error);
  }

  async reportMultipleRedemption(barcode: string) {
    const info = await this.transporter.sendMail({
      from: '"Coca-Cola" <redeemprojectcola@gmail.com>',
      to: 'redeemprojectcola@gmail.com',
      subject: 'Multiple Redemption Report',
      text: `${barcode} was reported for multiple redemption attempts`,
    });
  }

  async submitIssue(issueObj: { name: string; email: string; issue: string }) {
    const info = await this.transporter.sendMail({
      from: `"${issueObj.name}" ${issueObj.email}`,
      to: 'redeemprojectcola@gmail.com',
      subject: 'Issue',
      text: `${issueObj.issue}`,
    });
  }
}
