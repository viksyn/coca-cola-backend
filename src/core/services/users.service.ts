import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { Role } from '../../data/entities/role';
import { User } from '../../data/entities/user';
import { UserLoginDTO } from '../../models/user/user-login-dto';
import { JwtPayload } from '../interfaces/jwt-payload';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Role) private readonly rolesRepository: Repository<Role>,
  ) {}

  async signIn(user: UserLoginDTO): Promise<User | undefined> {
    const foundUser = await this.userRepository.findOne({
      where: {
        email: user.email,
      },
    });

    if (!foundUser || foundUser.isDeleted === true) {
      return null;
    }
    const passCheck = await bcrypt.compare(user.password, foundUser.password);

    if (passCheck) {
      return foundUser;
    }
    return null;
  }

  async validate(payload: JwtPayload): Promise<User | undefined> {
    return await this.userRepository.findOne({
      where: {
        ...payload,
      },
    });
  }
}
