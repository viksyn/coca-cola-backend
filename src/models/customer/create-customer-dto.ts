import { IsString, Length } from 'class-validator';

export class CreateCustomerDTO {

  @IsString()
  @Length(3, 20)
  brandName: string;

}
