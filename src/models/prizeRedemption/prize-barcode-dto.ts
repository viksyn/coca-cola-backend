import { IsString, Matches } from 'class-validator';

export class PrizeBarcodeDTO {
  @IsString()
  @Matches(/^[a-zA-Z0-9]*$/)
  barcode: string;
  @IsString()
  @Matches(/^[a-zA-Z0-9]*$/)
  prizeBarcode: string;
}
