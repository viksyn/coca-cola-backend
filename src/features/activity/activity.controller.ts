import { Controller, UseGuards, Get, HttpCode, HttpStatus } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ControllerRolesGuard } from '../../common/guards/controller.roles.guard';
import { Roles } from '../../common/decorators/roles.decorator';
import { UserRole } from '../../common/enums/user-role.enum';
import { ActivityService } from './activity.service';

@Controller('activity')
@UseGuards(AuthGuard(), ControllerRolesGuard)
@Roles(UserRole.Admin)
export class ActivityController {

  public constructor(private readonly activityService: ActivityService) { }

  @Get()
  @HttpCode(HttpStatus.OK)
  async getActivityRecord() {
    return this.activityService.getActivityRecord();
  }

}
