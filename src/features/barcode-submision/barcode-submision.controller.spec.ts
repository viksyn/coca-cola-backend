import { Test, TestingModule } from '@nestjs/testing';
import { BarcodeSubmisionController } from './barcode-submision.controller';

describe('BarcodeSubmision Controller', () => {
  let controller: BarcodeSubmisionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BarcodeSubmisionController],
    }).compile();

    controller = module.get<BarcodeSubmisionController>(BarcodeSubmisionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
