import {
  Body,
  Controller,
  Post,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { getUser } from '../../common/decorators/user.decorator';
import { BarcodeDTO } from '../../models/barcode/barcode-dto';
import { PrizeBarcodeDTO } from '../../models/prizeRedemption/prize-barcode-dto';
import { BarcodeSubmisionService } from './barcode-submision.service';

@Controller('barcode')
export class BarcodeSubmisionController {
  public constructor(
    private readonly barcodeSubmisionService: BarcodeSubmisionService,
  ) {}

  @Post()
  @UseGuards(AuthGuard())
  async submitBarcode(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    barcode: BarcodeDTO,
    @getUser() user: any,
  ): Promise<{}> {
    // TODO return a valid DTO

    return await this.barcodeSubmisionService.submitBarcode(barcode, user);
  }
  @Post('redeem')
  @UseGuards(AuthGuard())
  async redeemPrize(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    barcodes: PrizeBarcodeDTO,
    @getUser() user: any,
  ): Promise<{}> {
    // TODO return a valid DTO
    return await this.barcodeSubmisionService.redeemPrize(barcodes, user);
  }

  @Post('report')
  @UseGuards(AuthGuard())
  async reportMultipleRedemption(
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    barcode: BarcodeDTO,
    @getUser() user: any,
  ) {
    // TODO return a valid DTO
    return await this.barcodeSubmisionService.reportMultipleRedemption(barcode);
  }
}
