import { Module } from '@nestjs/common';
import { AuthModule } from '../../auth/auth.module';
import { CoreModule } from '../../core/core.module';
import { BarcodeSubmisionController } from './barcode-submision.controller';
import { BarcodeSubmisionService } from './barcode-submision.service';

@Module({
  controllers: [BarcodeSubmisionController],
  providers: [BarcodeSubmisionService],
  imports: [CoreModule, AuthModule],
})
export class BarcodeSubmisionModule {}
