import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Roles } from '../../common/decorators/roles.decorator';
import { getUser } from '../../common/decorators/user.decorator';
import { UserRole } from '../../common/enums/user-role.enum';
import { ControllerRolesGuard } from '../../common/guards/controller.roles.guard';
import { User } from '../../data/entities/user';
import { CreateOutletDTO } from '../../models/outlet/create-outlet-dto';
import { OutletService } from './outlet.service';

@Controller('customer')
@UseGuards(AuthGuard(), ControllerRolesGuard)
@Roles(UserRole.Admin)
export class OutletController {
  public constructor(private readonly outletService: OutletService) {}

  @Get(':customerId/outlet/:outletId')
  @HttpCode(HttpStatus.OK)
  async getCurrentOutlet(
    @Param('customerId') customerId: string,
    @Param('outletId') outletId: string,
  ) {
    return await this.outletService.getCurrentOutlet(customerId, outletId);
  }

  @Get(':customerId/outlets')
  @HttpCode(HttpStatus.OK)
  async getAllOutletsByCustomer(@Param('customerId') customerId: string) {
    return await this.outletService.getAllOutletsByCustomer(customerId);
  }

  @Post(':customerId/outlet')
  @HttpCode(HttpStatus.CREATED)
  async createOutlet(
    @Param('customerId') customerId: string,
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    outlet: CreateOutletDTO,
    @getUser() user: User,
  ) {
    return await this.outletService.createOutlet(customerId, outlet, user);
  }

  @Put(':customerId/outlet/:outletId')
  @HttpCode(HttpStatus.OK)
  async updateOutlet(
    @Param('customerId') customerId: string,
    @Param('outletId') outletId: string,
    @Body(new ValidationPipe({ whitelist: true, transform: true }))
    outlet: CreateOutletDTO,
    @getUser() user: User,
  ) {
    return await this.outletService.updateOutlet(
      customerId,
      outletId,
      outlet,
      user,
    );
  }

  @Delete(':customerId/outlet/:outletId')
  @HttpCode(HttpStatus.OK)
  async deleteOutlet(
    @Param('customerId') customerId: string,
    @Param('outletId') outletId: string,
    @getUser() user: User,
  ) {
    return await this.outletService.deleteOutlet(customerId, outletId, user);
  }
}
