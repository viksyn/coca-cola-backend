import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ActivityType } from '../../common/enums/activity-type.enum';
import { FeatureType } from '../../common/enums/feature-type.enum';
import { MappingService } from '../../core/services/mapping.service';
import { Customer } from '../../data/entities/customer';
import { Outlet } from '../../data/entities/outlet';
import { User } from '../../data/entities/user';
import { CreateOutletDTO } from '../../models/outlet/create-outlet-dto';
import { ShowOutletDTO } from '../../models/outlet/show-outlet-dto';
import { ActivityService } from '../activity/activity.service';

@Injectable()
export class OutletService {
  public constructor(
    @InjectRepository(Customer)
    private readonly customerRepository: Repository<Customer>,
    @InjectRepository(Outlet)
    private readonly outletRepository: Repository<Outlet>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly activityService: ActivityService,
    private readonly mappingService: MappingService,
  ) {}

  async getCurrentOutlet(
    customerId: string,
    outletId: string,
  ): Promise<ShowOutletDTO> {
    const customer = await this.customerRepository.findOne({
      where: {
        id: customerId,
        isDeleted: false,
      },
    });

    if (!customer) {
      throw new NotFoundException(
        'There is no such customer! Please try another criteria or create one first!',
      );
    }

    const outlet = await this.outletRepository.findOne({
      where: {
        customer,
        id: outletId,
        isDeleted: false,
      },
    });

    if (!outlet) {
      throw new NotFoundException(
        'There is no such outlet! Please try another criteria or create one first!',
      );
    }

    return await this.mappingService.returnOutlet(outlet);
  }

  async getAllOutletsByCustomer(customerId: string) {
    const customer = await this.customerRepository.findOne({
      where: {
        id: customerId,
        isDeleted: false,
      },
    });

    if (!customer) {
      throw new NotFoundException(
        'There is no such customer! Please try another criteria or create one first!',
      );
    }

    const outlets = await this.outletRepository.find({
      where: {
        brand: customer.id,
        isDeleted: false,
      },
    });

    if (outlets.length === 0) {
      throw new NotFoundException(
        'There are no outlets yet! You need to create one first!',
      );
    }

    const mappedOutlets = await outlets.map(outlet =>
      this.mappingService.returnOutlet(outlet),
    );

    return await Promise.all(mappedOutlets);
  }

  async createOutlet(
    customerId: string,
    outlet: CreateOutletDTO,
    user: User,
  ): Promise<ShowOutletDTO> {
    const customer = await this.customerRepository.findOne({
      where: {
        id: customerId,
        isDeleted: false,
      },
    });

    if (!customer) {
      throw new NotFoundException(
        'There is no such customer! Please try another criteria or create one first!',
      );
    }

    const checkExistingOutlet = await this.outletRepository.findOne({
      where: {
        location: outlet.location,
        brand: customer,
      },
    });

    if (checkExistingOutlet && checkExistingOutlet.isDeleted === false) {
      throw new NotFoundException('Outlet already exists');
    }
    if (checkExistingOutlet) {
      checkExistingOutlet.isDeleted = false;
      const editedIsDeletedOutlet = await this.customerRepository.save(
        checkExistingOutlet,
      );
      return await this.mappingService.returnOutlet(editedIsDeletedOutlet);
    }

    const newOutlet = await this.outletRepository.create();
    newOutlet.brand = Promise.resolve(customer);
    newOutlet.location = outlet.location;

    const savedNewOutlet = await this.outletRepository.save(newOutlet);

    if (savedNewOutlet && Object.keys(savedNewOutlet).length !== 0) {
      await this.activityService.createActivityRecord(
        user.id,
        user.name,
        FeatureType.outlet,
        ActivityType.CREATED,
        newOutlet.id,
        newOutlet.location,
      );
    }

    return await this.mappingService.returnOutlet(savedNewOutlet);
  }

  async updateOutlet(
    customerId: string,
    outletId: string,
    outlet: CreateOutletDTO,
    user: User,
  ) {
    const customer = await this.customerRepository.findOne({
      where: {
        id: customerId,
        isDeleted: false,
      },
    });

    if (!customer) {
      throw new NotFoundException(
        'There is no such customer! Please try another criteria or create one first!',
      );
    }

    const outletToEdit = await this.outletRepository.findOne({
      where: {
        id: outletId,
        brand: customer.id,
        isDeleted: false,
      },
    });

    if (!outletToEdit) {
      throw new NotFoundException(
        'There is no such outlet! Please try another criteria or create one first!',
      );
    }

    outletToEdit.location = outlet.location;

    const updatedOutlet = await this.outletRepository.save(outletToEdit);

    if (updatedOutlet && Object.keys(updatedOutlet).length !== 0) {
      await this.activityService.createActivityRecord(
        user.id,
        user.name,
        FeatureType.outlet,
        ActivityType.UPDATED,
        outletToEdit.id,
        outletToEdit.location,
      );
    }

    return await updatedOutlet;
  }

  async deleteOutlet(customerId: string, outletId: string, user: User) {
    const customer = await this.customerRepository.findOne({
      where: {
        id: customerId,
        isDeleted: false,
      },
    });

    if (!customer) {
      throw new NotFoundException(
        'There is no such customer! Please try another criteria or create one first!',
      );
    }

    const outlet = await this.outletRepository.findOne({
      where: {
        id: outletId,
        customer,
        isDeleted: false,
      },
    });

    if (!outlet) {
      throw new NotFoundException(
        'There is no such outlet! Please try another criteria or create one first!',
      );
    }

    outlet.isDeleted = true;

    const deletedOutlet = await this.outletRepository.save(outlet);

    if (deletedOutlet && Object.keys(deletedOutlet).length !== 0) {
      const outletUsers = await this.userRepository.find({
        where: {
          deletedOutlet,
          isDeleted: false,
        },
      });

      outletUsers.map(async outletUser => {
        outletUser.isDeleted = true;
        await this.userRepository.save(outletUser);
      });

      await this.activityService.createActivityRecord(
        user.id,
        user.name,
        FeatureType.outlet,
        ActivityType.DELETED,
        outlet.id,
        outlet.location,
      );
    }

    return await deletedOutlet;
  }
}
