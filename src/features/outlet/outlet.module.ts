import { Module } from '@nestjs/common';
import { AuthModule } from '../../auth/auth.module';
import { CoreModule } from '../../core/core.module';
import { ActivityModule } from '../activity/activity.module';
import { OutletController } from './outlet.controller';
import { OutletService } from './outlet.service';
import { MappingService } from '../../core/services/mapping.service';

@Module({
  controllers: [OutletController],
  providers: [OutletService, MappingService],
  imports: [CoreModule, AuthModule, ActivityModule],
})
export class OutletModule {}
