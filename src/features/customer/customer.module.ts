import { Module } from '@nestjs/common';
import { AuthModule } from '../../auth/auth.module';
import { CoreModule } from '../../core/core.module';
import { MappingService } from '../../core/services/mapping.service';
import { ActivityModule } from '../activity/activity.module';
import { OutletService } from '../outlet/outlet.service';
import { UserService } from '../user/user.service';
import { CustomerController } from './customer.controller';
import { CustomerService } from './customer.service';

@Module({
  controllers: [CustomerController],
  providers: [CustomerService, MappingService, UserService, OutletService],
  imports: [CoreModule, AuthModule, ActivityModule],
})
export class CustomerModule {}
