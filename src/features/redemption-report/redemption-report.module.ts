import { Module } from '@nestjs/common';
import { AuthModule } from '../../auth/auth.module';
import { CoreModule } from '../../core/core.module';
import { MappingService } from '../../core/services/mapping.service';
import { RedemptionReportController } from './redemption-report.controller';
import { RedemptionReportService } from './redemption-report.service';

@Module({
  controllers: [RedemptionReportController],
  providers: [RedemptionReportService, MappingService],
  imports: [CoreModule, AuthModule],
})
export class RedemptionReportModule {}
