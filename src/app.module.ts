import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { CoreModule } from './core/core.module';
import { ActivityModule } from './features/activity/activity.module';
import { BarcodeSubmisionModule } from './features/barcode-submision/barcode-submision.module';
import { CustomerModule } from './features/customer/customer.module';
import { OutletModule } from './features/outlet/outlet.module';
import { RedemptionReportModule } from './features/redemption-report/redemption-report.module';
import { UserModule } from './features/user/user.module';
import { UserService } from './features/user/user.service';
import { MappingService } from './core/services/mapping.service';

@Module({
  imports: [
    CoreModule,
    AuthModule,
    ConfigModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: configService.dbType as any,
        host: configService.dbHost,
        port: configService.dbPort,
        username: configService.dbUsername,
        password: configService.dbPassword,
        database: configService.dbName,
        entities: ['./src/data/entities/*.ts'],
      }),
    }),
    BarcodeSubmisionModule,
    RedemptionReportModule,
    CustomerModule,
    OutletModule,
    UserModule,
    ActivityModule,
  ],
  controllers: [AppController],
  providers: [],
}) 
export class AppModule {}
